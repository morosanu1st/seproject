import Pyro4
import Pyro4.naming


@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class RepressorProtein:
    # constructor
    def __init__(self):
        # amount of repressor protein,
        # r : [0..N] init 0
        self.__r = 0

    def get_r(self):
        return self.__r

    def set_r(self, r):
        self.__r = r

    def inc_r(self):
        self.__r += 1

    def dec_r(self):
        self.__r -= 1


repressorProtein = RepressorProtein()

ns = Pyro4.naming.locateNS(host="simulator")
daemon = Pyro4.Daemon(host="repressorprotein")
uri = daemon.register(repressorProtein)
print(uri)
ns.register("repressorProtein", uri)
daemon.requestLoop()