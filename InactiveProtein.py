import Pyro4
import Pyro4.naming


@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class InactiveProtein:

    # constructor
    def __init__(self):
        # amount of inactivated protein,
        # c : [0..N] init 0
        self.__c = 0

    def get_c(self):
        return self.__c

    def set_c(self, c):
        self.__c = c

    def inc_c(self):
        self.__c += 1

    def dec_c(self):
        self.__c -= 1

inactiveProtein=InactiveProtein()

ns = Pyro4.naming.locateNS(host="simulator")
daemon = Pyro4.Daemon(host="inactiveprotein")
uri = daemon.register(inactiveProtein)
print(uri)
ns.register("inactiveProtein", uri)
daemon.requestLoop()