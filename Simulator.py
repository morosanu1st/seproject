import rate
import random
import time
import Pyro4
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


activatorGene = Pyro4.Proxy("PYRONAME:activatorGene")
repressorGene = Pyro4.Proxy("PYRONAME:repressorGene")
activatorProtein = Pyro4.Proxy("PYRONAME:activatorProtein")
repressorProtein = Pyro4.Proxy("PYRONAME:repressorProtein")
activatorMRNA = Pyro4.Proxy("PYRONAME:activatorMRNA")
repressorMRNA = Pyro4.Proxy("PYRONAME:repressorMRNA")
inactiveProtein = Pyro4.Proxy("PYRONAME:inactiveProtein")

data=[]

class Simulator:
    # constructor
    def __init__(self, activator_gene, activator_mrna, activator_protein,
                 repressor_gene, repressor_mrna, repressor_protein, inactive_protein):
        # link to every client
        self.activator_gene = activator_gene
        self.activator_mrna = activator_mrna
        self.activator_protein = activator_protein
        self.repressor_gene = repressor_gene
        self.repressor_mrna = repressor_mrna
        self.repressor_protein = repressor_protein
        self.inactive_protein = inactive_protein

    # method to be called periodically to update system's state
    def update_system_state(self):
        # obtain all actions that can be executed together with their rates
        executable_actions = self.collect_executable_actions()

        # choose randomly(based on rates) an action and execute it
        self.choose_and_execute(executable_actions)

    # chooses an action randomly(based on rates) and performs it
    def choose_and_execute(self, available_transitions):
        sum_of_rates = 0.0
        for i in range(len(available_transitions)):
            sum_of_rates += available_transitions[i][1]

        r = random.uniform(1, sum_of_rates)
        acc = 0.0
        for t in available_transitions:
            acc += t[1]
            if r <= acc:
                self.perform(t[0], t[1])
                break

    # returns the list of [transition, rate] transitions that may be taken
    def collect_executable_actions(self):
        # collect data from all clients
        da = self.activator_gene.get_da()
        da_a = self.activator_gene.get_da_a()
        ma = self.activator_mrna.get_ma()
        a = self.activator_protein.get_a()
        dr = self.repressor_gene.get_dr()
        dr_a = self.repressor_gene.get_dr_a()
        mr = self.repressor_mrna.get_mr()
        r = self.repressor_protein.get_r()
        c = self.inactive_protein.get_c()
        transitions = []

        # build the list of all transitions that may be taken together with their rates
        if self.cond_transc_da(da, ma):
            transitions.append([self.take_transc_da, rate.transc_da])
        if self.cond_transc_da_a(da_a, ma):
            transitions.append([self.take_transc_da_a, rate.transc_da_a])
        if self.cond_bind_a(da, da_a, a):
            transitions.append([self.take_bind_a, rate.bind_a * a])
        if self.cond_rel_a(da, da_a, a):
            transitions.append([self.take_rel_a, rate.rel_a])
        if self.cond_transl_a(ma, a):
            transitions.append([self.take_transl_a, rate.transl_a * ma])
        if self.cond_deg_ma(ma):
            transitions.append([self.take_deg_ma, rate.deg_ma * ma])
        if self.cond_bind_r(a, dr, dr_a):
            transitions.append([self.take_bind_r, rate.bind_r * a])
        if self.cond_rel_r(a, dr, dr_a):
            transitions.append([self.take_rel_r, rate.rel_r])
        if self.cond_deg_a(a):
            transitions.append([self.take_deg_a, rate.deg_a * a])
        if self.cond_deactive(a, r, c):
            transitions.append([self.take_deactive, rate.deactive * a * r * c])
        if self.cond_transc_dr(dr, mr):
            transitions.append([self.take_transc_dr, rate.transc_dr])
        if self.cond_transc_dr_a(dr_a, mr):
            transitions.append([self.take_transc_dr_a, rate.transc_dr_a])
        if self.cond_transl_r(mr, r):
            transitions.append([self.take_transl_r, rate.transl_r * mr])
        if self.cond_deg_mr(mr):
            transitions.append([self.take_deg_mr, rate.deg_mr * mr])
        if self.cond_deg_r(r):
            transitions.append([self.take_deg_r, rate.deg_r * r])
        if self.cond_deg_c(r, c):
            transitions.append([self.take_deg_c, rate.deg_c * c])

        return transitions

    # executes function 'fun' that takes argumets '*args'
    @staticmethod
    def perform(fun, *args):
        print(fun.__name__)
        fun(*args)
	data.append(activatorProtein.get_a())

    ##################################################################

    def take_transc_da(self, total_rate):
        self.activator_mrna.inc_ma()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_transc_da_a(self, total_rate):
        self.activator_mrna.inc_ma()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_bind_a(self, total_rate):
        self.activator_gene.set_da(0)
        self.activator_gene.set_da_a(1)
        self.activator_protein.dec_a()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_rel_a(self, total_rate):
        self.activator_gene.set_da(1)
        self.activator_gene.set_da_a(0)
        self.activator_protein.inc_a()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_transl_a(self, total_rate):
        self.activator_protein.inc_a()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_deg_ma(self, total_rate):
        self.activator_mrna.dec_ma()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_bind_r(self, total_rate):
        self.activator_protein.dec_a()
        self.repressor_gene.set_dr(0)
        self.repressor_gene.set_dr_a(1)
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_rel_r(self, total_rate):
        self.activator_protein.inc_a()
        self.repressor_gene.set_dr(1)
        self.repressor_gene.set_dr_a(0)
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_deg_a(self, total_rate):
        self.activator_protein.dec_a()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_deactive(self, total_rate):
        self.activator_protein.dec_a()
        self.repressor_protein.dec_r()
        self.inactive_protein.dec_c()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_transc_dr(self, total_rate):
        self.repressor_mrna.inc_mr()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_transc_dr_a(self, total_rate):
        self.repressor_mrna.inc_mr()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_transl_r(self, total_rate):
        self.repressor_protein.inc_r()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_deg_mr(self, total_rate):
        self.repressor_mrna.dec_mr()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_deg_r(self, total_rate):
        self.repressor_protein.dec_r()
        time.sleep(1 / (total_rate * rate.delay_const))

    def take_deg_c(self, total_rate):
        self.repressor_protein.inc_r()
        self.inactive_protein.dec_c()
        time.sleep(1 / (total_rate * rate.delay_const))

    ##################################################################

    @staticmethod
    def cond_transc_da(da, ma):
        return da == 1 and ma < rate.N

    @staticmethod
    def cond_transc_da_a(da_a, ma):
        return da_a == 1 and ma < rate.N

    @staticmethod
    def cond_bind_a(da, da_a, a):
        return da == 1 and da_a == 0 and a > 0

    @staticmethod
    def cond_rel_a(da, da_a, a):
        return da == 0 and da_a == 1 and a < rate.N

    @staticmethod
    def cond_transl_a(ma, a):
        return ma > 0 and a < rate.N

    @staticmethod
    def cond_deg_ma(ma):
        return ma > 0

    @staticmethod
    def cond_bind_r(a, dr, dr_a):
        return a > 0 and dr == 1 and dr_a == 0

    @staticmethod
    def cond_rel_r(a, dr, dr_a):
        return a < rate.N and dr == 0 and dr_a == 1

    @staticmethod
    def cond_deg_a(a):
        return a > 0

    @staticmethod
    def cond_deactive(a, r, c):
        return a > 0 and r > 0 and c > 0

    @staticmethod
    def cond_transc_dr(dr, mr):
        return dr == 1 and mr < rate.N

    @staticmethod
    def cond_transc_dr_a(dr_a, mr):
        return dr_a == 1 and mr < rate.N

    @staticmethod
    def cond_transl_r(mr, r):
        return mr > 0 and r < rate.N

    @staticmethod
    def cond_deg_mr(mr):
        return mr > 0

    @staticmethod
    def cond_deg_r(r):
        return r > 0

    @staticmethod
    def cond_deg_c(r, c):
        return r < rate.N and c > 0


simulator = Simulator(activatorGene, activatorMRNA, activatorProtein, repressorGene, repressorMRNA, repressorProtein,
                      inactiveProtein)
a = input("For how many steps should it run?\n")
a = int(a)
for i in range(0, a):
    simulator.update_system_state()

t=np.arange(0., 200., 200.0/len(data))
plt.plot(t,data)
plt.savefig("plot"+str(a)+".png")
