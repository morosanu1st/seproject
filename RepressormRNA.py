import Pyro4
import Pyro4.naming


@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class RepressormRNA:

    # constructor
    def __init__(self):
        # amount of repressor mRNA,
        # mr : [0..N] init 0
        self.__mr = 0

    def get_mr(self):
        return self.__mr

    def set_mr(self, mr):
        self.__mr = mr

    def inc_mr(self):
        self.__mr += 1

    def dec_mr(self):
        self.__mr -= 1

repressorMRNA= RepressormRNA()

ns = Pyro4.naming.locateNS(host="simulator")
daemon = Pyro4.Daemon(host="repressormrna")
uri = daemon.register(repressorMRNA)
print(uri)
ns.register("repressorMRNA", uri)
daemon.requestLoop()