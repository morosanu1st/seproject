import Pyro4
import Pyro4.naming


@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class RepressorGene:

    # constructor
    def __init__(self):
        # amount of repressor gene,
        # dr : [0..1] init 1
        self.__dr = 1
        # amount of repressor gene with activator protein bound,
        # dr_a : [0..1] init 0
        self.__dr_a = 0

    def get_dr(self):
        return self.__dr

    def set_dr(self, dr):
        self.__dr = dr

    def get_dr_a(self):
        return self.__dr_a

    def set_dr_a(self, dr_a):
        self.__dr_a = dr_a

repressorGene=RepressorGene()

ns = Pyro4.naming.locateNS(host="simulator")
daemon = Pyro4.Daemon(host="repressorgene")
uri = daemon.register(repressorGene)
print(uri)
ns.register("repressorGene", uri)
daemon.requestLoop()