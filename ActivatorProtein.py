import Pyro4
import Pyro4.naming


@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class ActivatorProtein:
    # constructor
    def __init__(self):
        # amount of activator protein,
        # a : [0..N] init 0
        self.__a = 0

    def get_a(self):
        return self.__a

    def set_a(self, a):
        self.__a = a

    def inc_a(self):
        self.__a += 1

    def dec_a(self):
        self.__a -= 1


activatorProtein = ActivatorProtein()

ns = Pyro4.naming.locateNS(host="simulator")
daemon = Pyro4.Daemon(host="activatorprotein")
uri = daemon.register(activatorProtein)
print(uri)
ns.register("activatorProtein", uri)
daemon.requestLoop()
