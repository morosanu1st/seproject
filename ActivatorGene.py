import Pyro4
import Pyro4.naming


@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class ActivatorGene:
    # constructor
    def __init__(self):
        # amount of activator gene,
        # da : [0..1] init 1
        self.__da = 1
        # amount of activator gene with activator protein bound,
        # da_a : [0..1] init 0
        self.__da_a = 0

    def get_da(self):
        return self.__da

    def set_da(self, da):
        self.__da = da

    def get_da_a(self):
        return self.__da_a

    def set_da_a(self, da_a):
        self.__da_a = da_a


activatorGene = ActivatorGene()

ns = Pyro4.naming.locateNS(host="simulator")
daemon = Pyro4.Daemon(host="activatorgene")
uri = daemon.register(activatorGene)
print(uri)
ns.register("activatorGene", uri)
daemon.requestLoop()
