# rates module - implemented as a class of constants
import sys


class _rate:
    class RateError(TypeError): pass

    def __setattr__(self, name, value):
        if self.__dict__.has_key(name):
            raise self.RateError, "Can't rebind rate(%s)" % name
        self.__dict__[name] = value


sys.modules[__name__] = _rate()

import rate
rate.transc_da = 50
rate.transc_da_a = 500
rate.transc_dr = 0.01
rate.transc_dr_a = 50
rate.transl_a = 50
rate.transl_r = 5
rate.bind_a = 1
rate.bind_r = 1
rate.deactive = 2
rate.rel_a = 50
rate.rel_r = 100
rate.deg_a = 1
rate.deg_c = 1
rate.deg_r = 0.2
rate.deg_ma = 10
rate.deg_mr = 0.5
rate.delay_const = 10
rate.N = 1000000
