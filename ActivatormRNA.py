import Pyro4
import Pyro4.naming


@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class ActivatormRNA:
    # constructor
    def __init__(self):
        # amount of activator mRNA,
        # ma : [0..N] init 0
        self.__ma = 0

    def get_ma(self):
        return self.__ma

    def set_ma(self, ma):
        self.__ma = ma

    def inc_ma(self):
        self.__ma += 1

    def dec_ma(self):
        self.__ma -= 1


activatorMRNA = ActivatormRNA()

ns = Pyro4.naming.locateNS(host="simulator")
daemon = Pyro4.Daemon(host="activatormrna")
uri = daemon.register(activatorMRNA)
print(uri)
ns.register("activatorMRNA", uri)
daemon.requestLoop()
